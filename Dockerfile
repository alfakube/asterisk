FROM docker.io/fedora:30 AS build

RUN dnf update -y libcurl pcre krb5-libs glibc libcrypt-nss glibc-langpack-en openssl && \
    dnf install -y libgomp libtool-ltdl make libstdc++ binutils newt sqlite jansson binutils \
    unixODBC gmime libvorbis libogg speexdsp speex spandsp libsrtp \
    dmidecode gcc gcc-c++ patch subversion bzip2 \
    ncurses-devel libxml2-devel openssl-devel newt-devel kernel-devel \
    sqlite-devel libuuid-devel jansson-devel binutils-devel \
    unixODBC-devel libtool-ltdl-devel gmime-devel libvorbis-devel \
    libogg-devel speexdsp-devel speex-devel libcurl-devel spandsp-devel libsrtp-devel \
    postgresql-odbc langpacks-ru libtiff-tools mailx ssmtp sox curl wget libedit-devel && \
    wget -O - http://downloads.asterisk.org/pub/telephony/asterisk/old-releases/asterisk-16.7.0.tar.gz | tar -x -z && \
    cd asterisk-16.7.0 && curl -O http://usecallmanager.nz/includes/cisco-usecallmanager-16.7.0.patch && \
    patch -p1 -i ./cisco-usecallmanager-16.7.0.patch && ./contrib/scripts/get_mp3_source.sh && \
    ./configure --with-pjproject-bundled --prefix=/usr --libdir=/usr/lib64 && \
    wget -O menuselect.makeopts https://stolyarch.uk/ast160-menuselect.makeopts && make menuselect.makeopts && \
    make DEBUG= OPTIMIZE=-O && make install && make samples && cd .. && rm -rf asterisk-* && \
    wget -O - https://github.com/chan-sccp/chan-sccp/archive/v4.3.2-epsilon.tar.gz | tar xz && \
    cd chan-sccp-4.3.2-epsilon && ./configure --disable-debug --enable-conference && make && make install && cd .. && rm -rf chan-sccp-4.3.2-epsilon && \
    cd /usr/lib64/asterisk/modules && \
    wget -O codec_g723.so http://asterisk.hosting.lv/bin/codec_g723-ast160-gcc4-glibc-x86_64-core2-sse4.so && \
    wget -O codec_g729.so http://asterisk.hosting.lv/bin/codec_g729-ast160-gcc4-glibc-x86_64-core2-sse4.so && \
    cd /tmp && wget -O - https://downloads.digium.com/pub/telephony/codec_opus/asterisk-16.0/x86-64/codec_opus-16.0_current-x86_64.tar.gz | tar -x -z && \
    mv codec_opus-16.0_1.3.0-x86_64/*.so /usr/lib64/asterisk/modules && mv codec_opus-16.0_1.3.0-x86_64/*.xml /var/lib/asterisk/documentation/thirdparty && \
    rm -rf codec_opus-16.0_1.3.0-x86_64 && \
    echo "noload => chan_skinny.so" >> /etc/asterisk/modules.conf && \
    echo "noload => res_pjsip_transport_websocket.so" >> /etc/asterisk/modules.conf && \
    echo "noload => cel_sqlite3_custom.so" >> /etc/asterisk/modules.conf && \
    echo "noload => cdr_sqlite3_custom.so" >> /etc/asterisk/modules.conf && \
    echo "noload => app_page.so" >> /etc/asterisk/modules.conf && \
    echo "noload => res_hep_rtcp.so" >> /etc/asterisk/modules.conf && \
    echo "noload => res_hep_pjsip.so" >> /etc/asterisk/modules.conf && \
    echo "mailhub=zm.an-security.ru" > /etc/ssmtp/ssmtp.conf && \
    echo "root:voicemail@an-security.ru:zm.an-security.ru" > /etc/ssmtp/revaliases && \
    ldconfig -v

FROM docker.io/fedora:30
RUN dnf update -y libcurl pcre krb5-libs glibc libcrypt-nss glibc-langpack-en openssl curl && \
    dnf install -y libgomp libtool-ltdl make libstdc++ binutils newt sqlite jansson binutils \
    unixODBC gmime libvorbis libedit libogg speexdsp speex spandsp libsrtp postgresql-odbc glibc-langpack-ru \
    libtiff-tools mailx ssmtp sox wget && dnf clean all && ln -sf /usr/share/zoneinfo/Europe/Moscow /etc/localtime

COPY --from=build /usr/sbin/asterisk /usr/sbin/asterisk
COPY --from=build /usr/lib64/libasteriskssl.so /usr/lib64/libasteriskssl.so.1 /lib64/libasteriskpj.so /lib64/libasteriskpj.so.2 /usr/lib64/
COPY --from=build /usr/lib64/asterisk /usr/lib64/asterisk
COPY --from=build /var/lib/asterisk /var/lib/asterisk
COPY --from=build /var/spool/asterisk /var/spool/asterisk
COPY --from=build /etc/asterisk /etc/asterisk
COPY --from=build /etc/ssmtp /etc/ssmtp

ENV TZ=Europe/Moscow \
    LANG=ru_RU.utf8 \
    TERM=xterm-256color

CMD asterisk -fvv
